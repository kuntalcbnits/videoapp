const router = require('express').Router();
const mobileUserController = require("../../controllers/mobileUserSection/signUpLogin.controller");  // Controler path

const verifyToken = require('../../config/verifyToken');

router.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.append('Access-Control-Allow-Headers', 'Content-Type, Origin, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Headers, X-Auth-Token, Authorization, X-Requested-With');
    next();
});


router.post("/create", mobileUserController.createLiveVideoRoom);
router.get("/get/all/videoroom", mobileUserController.getAllVideoRoom);
router.post("/get/info", mobileUserController.getVideoRoomDetails);

module.exports = router;