const router = require('express').Router();
const vehicleTypeController = require('../../controllers/adminSection/vehicletype.controller');

router.get('/', vehicleTypeController.getAllVehicleTypes);
router.post('/', vehicleTypeController.createVehicleType);
router.get('/:id', vehicleTypeController.getVehicleTypeById);
router.post('/:id', vehicleTypeController.updateVehicleType);
router.delete('/:id', vehicleTypeController.deleteVehicleType);

module.exports = router;
