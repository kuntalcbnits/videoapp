const router = require("express").Router();
const cmsController = require("../../controllers/adminSection/cms.controller");

router.get('/', cmsController.getAllCMS);
router.post('/', cmsController.createCMS);
router.post('/:id', cmsController.updateCMS);
router.get('/:id', cmsController.getCMSById);
router.delete('/:id', cmsController.deleteCMS)

module.exports = router;
