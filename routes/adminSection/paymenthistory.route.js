const router = require('express').Router();
const paymentHistoryController = require('../../controllers/adminSection/paymenthistory.controller');

router.get('/', paymentHistoryController.getAllPaymentHistory);
router.post('/', paymentHistoryController.addPaymentHistory);
router.get('/:id', paymentHistoryController.getPaymentHistoryByDriver);

module.exports = router;
