const express = require('express');
const router = express.Router();

const imageUpload = require('../../config/imageUpload');
const userController = require("../../controllers/adminSection/user.controller");

router.get('/', userController.getAllUsers);
router.post('/', imageUpload.uploadUserImage, userController.createUser);
router.post('/:id', imageUpload.uploadUserImage, userController.updateUser);
router.get('/:id', userController.getUserById);
router.delete('/:id', userController.deleteUser);

module.exports = router;
