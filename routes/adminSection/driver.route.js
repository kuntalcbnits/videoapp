const router = require("express").Router();
const driverController = require("../../controllers/adminSection/driver.controller");
const upload = require('../../config/imageUpload');

router.get('/', driverController.getAllDrivers);
router.post('/', upload.uploadDriverImage, driverController.createDriver);
// router.post("/create", upload.uploadDriverImage, driverController.createDriver);
// router.post("/update", upload.uploadDriverImage, driverController.updateDriver);
// router.get("/read/:id", driverController.getDriverById);

router.get('/:id', driverController.getDriverById);
router.post('/:id', upload.uploadDriverImage, driverController.updateDriver);
router.delete('/:id', driverController.deleteDriver);

module.exports = router;
