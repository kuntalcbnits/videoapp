const router = require("express").Router();
const emailController = require("../../controllers/adminSection/email.controller");

router.get('/', emailController.getAllEmailTemplates);
router.post("/", emailController.createEmailTemplate);
router.post("/:id", emailController.updateEmailTemplate);
router.get("/:id", emailController.getEmailTemplateById);
router.delete('/:id', emailController.deleteEmailTemplate)

module.exports = router;
