const router = require('express').Router();
const adminController = require("../../controllers/adminSection/admin.controller");

const verifyToken = require('../../config/verifyToken');

router.post("/login", adminController.adminLogin);
router.post("/register", adminController.signupAdmin);
router.post("/forgot-password", adminController.forgotPassword);
router.post('/reset-password', adminController.resetPassword);

router.use('/email', verifyToken.verifyAdmin, require('./email.route'));
router.use('/cms', verifyToken.verifyAdmin, require('./cms.route'));
router.use('/user', verifyToken.verifyAdmin, require('./user.route'));
router.use('/driver', verifyToken.verifyAdmin, require('./driver.route'));
router.use('/setting', verifyToken.verifyAdmin, require('./setting.route'));
router.use('/ride-history', verifyToken.verifyAdmin, require('./ridehistory.route'));
router.use('/payment-history', verifyToken.verifyAdmin, require('./paymenthistory.route'));
router.use('/vehicle-type', verifyToken.verifyAdmin, require('./vehicletype.route'));
router.use('/vehicle', verifyToken.verifyAdmin, require('./vehicle.route'));
router.use('/subscription', verifyToken.verifyAdmin, require('./subscription.route'));

router.get('/:id', verifyToken.verifyAdmin, adminController.getAdminById);
router.post('/:id', verifyToken.verifyAdmin, adminController.updateProfile)

module.exports = router;
