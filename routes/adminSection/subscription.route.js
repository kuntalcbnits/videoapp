const router = require('express').Router(),
    subscriptionController = require('../../controllers/adminSection/subscription.controller');

router.get('/', subscriptionController.getAllSubscription);
router.post('/', subscriptionController.createSubscription);
router.get('/:id', subscriptionController.getSubscriptionById);
router.post('/:id', subscriptionController.updateSubscription);

module.exports = router
