const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const settingController = require("../../controllers/adminSection/setting.controller");
const upload = require('../../config/imageUpload');

router.get('/', upload.uploadSiteSettingImage, settingController.getSettings);
router.post('/', upload.uploadSiteSettingImage, settingController.createSetting);
router.post('/:id', upload.uploadSiteSettingImage, settingController.updateSetting)

module.exports = router;
