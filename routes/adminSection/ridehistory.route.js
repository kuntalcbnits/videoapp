const router = require('express').Router();
const rideHistoryController = require('../../controllers/adminSection/ridehistory.controller');

router.get('/', rideHistoryController.getAllRideHistory);
router.post('/', rideHistoryController.addRideHistory);
router.get('/:id', rideHistoryController.getRideHistoryByDriver);

module.exports = router;
