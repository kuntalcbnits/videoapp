const router = require('express').Router();

router.use('/driver', require('./driver.route'));
router.use('/ride-history', require('./ridehistory.route'));
router.use('/payment-history', require('./paymenthistory.route'));

module.exports = router;
