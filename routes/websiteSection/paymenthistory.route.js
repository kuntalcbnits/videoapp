const router = require("express").Router();
const paymentHistoryController = require("../../controllers/websiteSection/paymenthistory.controller");

router.get('/:id', paymentHistoryController.getDriverPaymentHistory);

module.exports = router;
