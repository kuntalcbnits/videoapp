const router = require("express").Router();
const driverController = require("../../controllers/websiteSection/driver.controller");
const upload = require('../../config/imageUpload');

router.get('/', driverController.getAllDrivers);
router.post("/register", upload.uploadDriverImage, driverController.createDriver);
router.post("/login", driverController.loginDriver)
router.post("/:id", upload.uploadDriverImage, driverController.updateDriver);
router.get("/:id", driverController.getDriverById);

module.exports = router;
