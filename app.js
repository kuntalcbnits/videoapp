// app.js
const debug = require('debug')('utaxiu-api:app')
const express = require('express');
const bodyParser = require('body-parser');
// initialize our express app
const app = express();

// CORS middleware
const cors = require('cors');

// load environment settings
require('dotenv').config();

global.__basedir = __dirname;

// Set up mongoose connection
const mongoose = require('mongoose');
let mongoDB = process.env.MONGODB_URI;
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static('resources'))

const adminRoutes = require('./routes/adminSection/admin.route'); // import routs for the admin section
const websiteRoutes = require('./routes/websiteSection/website.route'); // import routes for website section
const mobileUserRoutes = require('./routes/mobileUserSection/signUpLogin.route') ; // import routs for mobile user

app.use('/admin', adminRoutes);
app.use('/website', websiteRoutes);
app.use('/mobile/user/', mobileUserRoutes);

const port = process.env.PORT || 3000;
const host = process.env.HOST;
app.listen(port, host, function () {
    debug('Taxi App server is running on port %s:%s', host, port);
})
