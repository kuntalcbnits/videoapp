const PaymentHistory = require('../../models/paymenthistory.model');
const mongoose = require('mongoose');

exports.getDriverPaymentHistory = (req, res) => {
    let driverId = req.params.id;

    PaymentHistory
        .aggregate([
            {
                $match: {
                    driverId: mongoose.Types.ObjectId(driverId)
                }
            },
            {
                $lookup: {
                    from: 'drivers',
                    localField: 'driverId',
                    foreignField: '_id',
                    as: 'driverInfo'
                }
            },
            {
                $lookup: {
                    from: 'ridehistories',
                    localField: 'rideId',
                    foreignField: '_id',
                    as: 'rideInfo'
                }
            },
            {
                $unwind: "$driverInfo"
            },
            {
                $unwind: "$rideInfo"
            }
        ])
        .sort({ date: -1 })
        .then(result => {
            return res.status(200).send({ PaymentHistory: result }).end();
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
}
