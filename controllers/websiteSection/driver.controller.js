const Driver = require('../../models/driver.model');
const crypto = require('crypto');
const Joi = require("@hapi/joi");
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const config = require('../../config/config');
const key = config.secret;

// Inserting a user data into database during signup
exports.createDriver = function (req, res) {
    // console.log(req.files);
    // console.log(req.body);
    const schema = Joi.object({
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        phoneNo: Joi.string().required(),
        email: Joi.string().required(),
        address: Joi.string().required(),
        password: Joi.string().required(),
        status: Joi.boolean().required(),
        operatingHours: Joi.required()
    });
    const payload = req.body;
    const result = schema.validate(payload);
    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    } else {
        let encoded = encode(payload.password);
        let encrypted = encrypt(encoded);
        const newDriver = Driver({
            firstName: payload.firstName,
            lastName: payload.lastName,
            phoneNo: payload.phoneNo,
            email: payload.email,
            address: payload.address,
            password: encrypted,
            operatingHours: payload.operatingHours,
            personalID: config.driverImageDBPath + req.files['personalID'][0].filename,
            status: payload.status,
        });
        newDriver.save().then(() => {
            return res.status(201).send({ message: "Driver added successfully." });
        }).catch(err => {
            return res.status(500).send({ message: err.message });
        });
    }
};

exports.loginDriver = (req, res) => {
    const schema = Joi.object({
        email: Joi.string().required(),
        password: Joi.string().required()
    });
    const result = schema.validate(req.body);
    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    } else {
        Driver.findOne({ email: req.body.email, status: true }, function (err, driver) {
            if (!driver) {
                res.status(401).send({ message: "Invalid credentials" });
            } else {
                // console.log(driver)
                let encoded = encode(req.body.password);
                if (!isSame(encoded, driver.password)) {
                    res.status(401).send({ message: "invalid credentials" });
                } else {
                    let token = jwt.sign(
                        {
                            isDeleted: driver.isDeleted,
                            date: driver.date,
                            _id: driver._id,
                            firstName: driver.firstName,
                            lastName: driver.lastName,
                            phoneNo: driver.phoneNo,
                            email: driver.email,
                            address: driver.address,
                            personalID: driver.personalID,
                            dateOfBirth: driver.dateOfBirth,
                            details: driver.details,
                            drivingLicenceNo: driver.drivingLicenceNo,
                            educationalQualification: driver.educationalQualification,
                            gender: driver.gender,
                            personalIDNo: driver.personalIDNo,
                            drivingLicence: driver.drivingLicence,
                            profileImg: driver.profileImg
                        },
                        key,
                        { expiresIn: config.expiresIn }
                    );
                    res.set("x-authorization", "Bearer " + token);
                    res.status(200).send({
                        ack: 1,
                        message: "Login successfull",
                        token: token,
                        driverId: driver._id
                    });
                }
            }
        });
    }
};

exports.updateDriver = (req, res) => {
    const driverId = req.params.id;
    // console.log(req.body);
    const schema = Joi.object({
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        phoneNo: Joi.string().required(),
        email: Joi.string().required(),
        address: Joi.string().required(),
        // details: Joi.string().required(),
        // password: Joi.string().required(),
        dateOfBirth: Joi.date().required(),
        gender: Joi.string().required(),
        educationalQualification: Joi.string().required(),
        personalIDNo: Joi.string().required(),
        drivingLicenceNo: Joi.string().required(),
        // personalID: Joi.string().required(),
        // drivingLicence: Joi.string().required(),
        status: Joi.boolean().required(),
        isDeleted: Joi.boolean().required()
    });
    const payload = req.body;
    const result = schema.validate(payload);
    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    } else {
        Driver.findOne({
            _id: driverId,
            isDeleted: false
        }).then(result => {
            if (result) {
                let updateObj = {}
                if (Object.keys(req.files).length > 0) {
                    // console.log('files present');
                    updateObj = {
                        firstName: payload.firstName,
                        lastName: payload.lastName,
                        phoneNo: payload.phoneNo,
                        email: payload.email,
                        address: payload.address,
                        // details: payload.details,
                        // password: payload.,
                        dateOfBirth: payload.dateOfBirth,
                        gender: payload.gender,
                        educationalQualification: payload.educationalQualification,
                        personalIDNo: payload.personalIDNo,
                        drivingLicenceNo: payload.drivingLicenceNo,
                        profileImg: req.files["profileImg"] ? config.driverImageDBPath + req.files["profileImg"][0].filename : result.profileImg,
                        personalID: req.files["personalID"] ? config.driverImageDBPath + req.files["personalID"][0].filename : result.personalID,
                        drivingLicence: req.files["drivingLicence"] ? config.driverImageDBPath + req.files["drivingLicence"][0].filename : result.drivingLicence,
                        status: payload.status,
                        isDeleted: payload.isDeleted
                    }
                } else {
                    // console.log('files absent');
                    updateObj = {
                        firstName: payload.firstName,
                        lastName: payload.lastName,
                        phoneNo: payload.phoneNo,
                        email: payload.email,
                        address: payload.address,
                        // details: payload.details,
                        // password: payload.,
                        dateOfBirth: payload.dateOfBirth,
                        gender: payload.gender,
                        educationalQualification: payload.educationalQualification,
                        personalIDNo: payload.personalIDNo,
                        drivingLicenceNo: payload.drivingLicenceNo,
                        status: payload.status,
                        isDeleted: payload.isDeleted
                    }
                }
                Driver.findOneAndUpdate(
                    {
                        _id: driverId
                    },
                    {
                        $set: updateObj
                    }
                ).then(updated => {
                    return res.status(200).send({ message: "Driver updated successfully.", Driver: updated });
                }).catch(err => {
                    return res.status(400).send({ message: err.message });
                });
            } else {
                return res.status(404).send({ message: "CMS Not Found." });
            }
        }).catch(err => {
            return res.status(500).send({ message: err.message });
        });
    }
};

exports.getAllDrivers = (req, res) => {
    Driver.find({
        isDeleted: false
    }).then(results => {
        if (results.length == 0) {
            return res.status(404).send({ message: "Drivers Not Found." });
        }
        return res.status(200).send({ message: "Driver details found", DriverList: results });
    }).catch(err => {
        return res.status(400).send({ message: err.message });
    });
};

exports.getDriverById = (req, res) => {
    Driver.findOne({
        _id: req.params.id,
        isDeleted: false
    }).then(result => {
        if (!result) {
            return res.status(404).send({ message: "Driver Not Found." });
        }
        return res.status(200).send({ message: `Driver ID ${result._id}`, Driver: result });
    }).catch(err => {
        return res.status(500).send({ message: err.message });
    });
};

function encode(password) {
    return crypto.createHash('sha256').update(password).digest('hex');
}

function encrypt(hashString) {
    let salt = bcrypt.genSaltSync(12);

    return bcrypt.hashSync(hashString, salt);
}
function isSame(hexString, hashString) {
    return bcrypt.compareSync(hexString, hashString);
}
