const RideHistory = require('../../models/ridehistory.model');
const mongoose = require('mongoose');

exports.getDriverRideHistory = (req, res) => {
    let driverId = req.params.id;

    RideHistory
        .aggregate([
            {
                $match: {
                    driverId: mongoose.Types.ObjectId(driverId)
                }
            },
            {
                $lookup: {
                    from: 'drivers',
                    localField: 'driverId',
                    foreignField: '_id',
                    as: 'driverInfo'
                }
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'passengerId',
                    foreignField: '_id',
                    as: 'userInfo'
                }
            },
            {
                $unwind: "$driverInfo"
            },
            {
                $unwind: "$userInfo"
            }
        ])
        .sort({ date: -1 })
        .then(result => {
            return res.status(200).send({ RideHistory: result }).end();
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
}
