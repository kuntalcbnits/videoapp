const config = require("../../config/config");
const VideoRoom = require("../../models/videoapp.model");  // Model path
const bcrypt = require("bcryptjs");
const key = config.secret;
const jwt = require("jsonwebtoken");
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const otpGenerator = require('otp-generator')


exports.createLiveVideoRoom = (req, res, next) => {
    try
    {
        newid = otpGenerator.generate(10, { specialChars: false });
        let roomId = newid;
        let roomName = req.body.roomName;
        let roomTitle = req.body.roomTitle;
        let roomKey = req.body.roomKey;
        let roomStatus = true;
        let createDate = Date.now();

        const videoroom = new VideoRoom({
            roomId,
            roomName,
            roomTitle,
            roomKey,
            roomStatus,
            createDate
        }).save()
        .then(videoroom =>{
            res.send({ack: 1, roomId: roomId});
        })
        .catch(e => {
            res.send({ ack: 2});
        });
    }
    catch (err) 
    {
        res.send({ack: 3});
    }
};


exports.getAllVideoRoom= (req, res, next) => {
    try
    {
        VideoRoom.find({}, function(err, users) {
            res.send({ack:1, videolist: users});
         });
    }
    catch (err) 
    {
        res.send({ack: 3});
    }
};


exports.getVideoRoomDetails= (req, res, next) => {
    try
    {
        let roomId = req.body.roomId;

        VideoRoom.findOne({
            roomId
        })
        .then(result => { 
            if(result==null)
            {
                res.send({ack:4});
            }
            else
            {
                res.send({ack:1, videoDetails: result});
            }
        })
        .catch(e => {
            res.send({ ack:5});
        });
    }
    catch (err) 
    {
        res.send({ack:3 });
    }
};
