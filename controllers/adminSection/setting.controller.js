const Setting = require('../../models/setting.model');
const crypto = require('crypto');
const Joi = require("@hapi/joi");
const bcrypt = require('bcryptjs');
const config = require('../../config/config');

// Inserting a user data into database during signup
exports.createSetting = function (req, res) {
    const schema = Joi.object({
        paypalEmail: Joi.string().required(),
        siteEmail: Joi.string().required(),
        mobile: Joi.string().required(),
        skype: Joi.string().required(),
        siteName: Joi.string().required(),
        address: Joi.string().required()
    });
    const payload = req.body
    const result = schema.validate(payload);
    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    } else {
        const newSetting = new Setting({
            paypalEmail: payload.paypalEmail,
            siteEmail: payload.siteEmail,
            mobile: payload.mobile,
            skype: payload.skype,
            siteName: payload.siteName,
            address: payload.address,
            iconFile: config.siteSettingDBPath + req.files["iconFile"][0].filename,
            logoFile: config.siteSettingDBPath + req.files["logoFile"][0].filename
        });
        newSetting.save().then(() => {
            return res.status(201).send({ message: "Setting added successfully." });
        }).catch(err => {
            return res.status(500).send({ message: err.message });
        });
    }
};

exports.updateSetting = (req, res) => {
    // console.log(req.body);
    // console.log(req.files["iconFile"], req.files["logoFile"]);
    let settingId = req.params.id;
    const schema = Joi.object({
        paypalEmail: Joi.string().required(),
        siteEmail: Joi.string().required(),
        mobile: Joi.string().required(),
        skype: Joi.string().required(),
        siteName: Joi.string().required(),
        address: Joi.string().required()
    });
    const result = schema.validate(req.body);
    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    } else {
        Setting.findOne({
            _id: settingId
        }).then(result => {
            if (result) {
                let updateObj = {};
                if (Object.keys(req.files).length > 0) {
                    // console.log('files present');
                    updateObj = {
                        paypalEmail: req.body.paypalEmail,
                        siteEmail: req.body.siteEmail,
                        mobile: req.body.mobile,
                        skype: req.body.skype,
                        siteName: req.body.siteName,
                        address: req.body.address,
                        iconFile: req.files["iconFile"] ? config.siteSettingDBPath + req.files["iconFile"][0].filename : req.body.iconFile,
                        logoFile: req.files["logoFile"] ? config.siteSettingDBPath + req.files["logoFile"][0].filename : req.body.logoFile
                    }
                } else {
                    updateObj = {
                        paypalEmail: req.body.paypalEmail,
                        siteEmail: req.body.siteEmail,
                        mobile: req.body.mobile,
                        skype: req.body.skype,
                        siteName: req.body.siteName,
                        address: req.body.address
                    }
                }
                Setting.findOneAndUpdate(
                    {
                        _id: settingId
                    },
                    {
                        $set: updateObj
                    }
                ).then(() => {
                    return res.status(200).send({ message: "Setting updated successfully." });
                }).catch(err => {
                    return res.status(400).send({ message: err.message });
                });
            } else {
                return res.status(404).send({ message: "Setting Not Found." });
            }
        }).catch(err => {
            return res.status(500).send({ message: err.message });
        });
    }
};

exports.getSettings = (req, res) => {
    Setting.find()
        .then(results => {
            if (results.length == 0) {
                return res.status(404).send({ message: "Setting Not Found." });
            }
            return res.status(200).send({ message: "Setting details found", Setting: results });
        }).catch(err => {
            return res.status(400).send({ message: err.message });
        });
};
