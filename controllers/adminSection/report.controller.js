const PaymentHistory = require('../../models/paymenthistory.model'),
      RideHistory    = require('../../models/ridehistory.model');

exports.generateReportByDriver = (req, res) => {
    let driverId = req.params.id;

    let from = req.query.from;
    let to   = req.query.to ? new Date(req.query.to) : new Date();

    PaymentHistory
        .aggregate([
            {
                $match: {
                    _id: driverId,
                    date: {
                        $lte: to,
                        $gte: from
                    }
                },
                $lookup: {
                    from: 'ridehistories',
                    localField: 'rideId',
                    foreignField: '_id',
                    as: 'rideDetails'
                },
                $unwind: '$rideDetails'
            }
        ])
        .then(history => {
            return res.status(200).send({ history }).end();
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
}
