const config = require("../../config/config");
const Joi = require("@hapi/joi");
const Admin = require("../../models/admin.model");
const bcrypt = require("bcryptjs");
const key = config.secret;
const jwt = require("jsonwebtoken");
const crypto = require('crypto');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const mailer = require('../../config/mailer');

exports.adminLogin = (req, res, next) => {
    // login logic
    let email = req.body.email;
    let password = req.body.password;
    const schema = Joi.object({
        email: Joi.string().required(),
        password: Joi.string().required()
    });
    const result = schema.validate(req.body);
    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    } else {
        Admin.findOne({ email }, function (err, admin) {
            if (!admin) {
                res.status(401).send({ ack: 0, message: "Invalid email. Please retry." });
            } else {
                // console.log(admin)
                let encoded = encode(password);
                if (!isSame(encoded, admin.password)) {
                    res.status(401).send({ ack: 0, message: "invalid credentials" });
                } else {
                    let token = jwt.sign(
                        {
                            _id: admin._id,
                            role: admin.role,
                            email: admin.email,
                            firstName: admin.firstName,
                            lastName: admin.lastName
                        },
                        key,
                        { expiresIn: config.expiresIn }
                    );
                    res.set("x-authorization", "Bearer " + token);
                    res.status(200).send({
                        ack: 1,
                        message: "Login successfull",
                        token: token,
                        adminId: admin._id
                    });
                }
            }
        });
    }
};

exports.signupAdmin = (req, res, next) => {
    const { password, email, firstName, lastName, phoneNo, address } = req.body;
    // console.log(req.body);
    const schema = Joi.object({
        email: Joi.string().required(),
        password: Joi.string().required(),
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        phoneNo: Joi.string().required(),
        address: Joi.string().required()
    });
    const result = schema.validate(req.body);
    // console.log(result, "ddddddd")

    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    } else {
        Admin.findOne({
            email
        }).then(result => {
            if (result) {
                res.status(401).send({
                    message:
                        "Admin already exist with this email, please try with another email.."
                });
            } else {
                let encoded = encode(password);
                let encrypted = encrypt(encoded);
                // bcrypt.hash(password, config.salt_round, function(err, hash) {
                const admin = new Admin({
                    password: encrypted,
                    email,
                    firstName,
                    lastName,
                    phoneNo,
                    address
                    // role :"subAdmin"
                }).save()
                    .then(admin => {
                        //console.log(admin)
                        res.status(201).send({ message: "Registered Successfully." });
                    })
                    .catch(e => {
                        res.status(500).send({ message: e.message });
                    });
                // });
            }
        }).catch(e => {
            res.status(500).send({ message: e.message });
        });
    }
};

exports.forgotPassword = (req, res) => {
    const schema = Joi.object({
        email: Joi.string().required()
    });
    const result = schema.validate(req.body);

    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    }
    Admin.findOne({ email: req.body.email })
        .then(admin => {
            if (!admin) {
                return res.status(404).send({ message: 'Admin Not Found.' });
            }
            // generate token
            const token = crypto.randomBytes(20).toString('hex');
            // store in db
            Admin.findOneAndUpdate(
                {
                    _id: admin._id
                },
                {
                    $set: {
                        resetPasswordToken: token,
                        tokenExpiration: Date.now() + 360000
                    }
                }
            ).then(async () => {
                // send mail to admin
                let url = config.resetPasswordBaseUrl + token;
                mailer.sendForgotPasswordMail(admin.email, url)
                    .then((mail) => {
                        return res.status(200).send({ message: "Reset Password Link Sent" })
                    }).catch(error => {
                        return res.status(400).send({ message: error });
                    });
            }).catch(err => {
                return res.status(400).send({ message: err.message });
            });
        })
};

exports.resetPassword = (req, res) => {
    const schema = Joi.object({
        token: Joi.string().required(),
        email: Joi.string().required(),
        password: Joi.string().required()
    });

    let result = schema.validate(req.body);

    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    }

    Admin.findOne({
        email: req.body.email
    }).then(admin => {
        // console.log('Admin', admin);
        if (Date.now() > admin.tokenExpiration) {
            return res.status(401).send({ message: "Link has expired" })
        }
        let encoded = encode(req.body.password);
        let encrypted = encrypt(encoded);
        // console.log('new password', encrypted);
        Admin.findOneAndUpdate(
            {
                _id: admin._id
            },
            {
                $set: {
                    password: encrypted,
                    resetPasswordToken: null,
                    tokenExpiration: null
                }
            }
        ).then(() => {
            return res.status(200).send({ message: "Password Reset Successful." });
        }).catch(error => {
            return res.status(500).send({ message: error.message });
        })
    }).catch(error => {
        return res.status(500).send({ message: error.message });
    });
};

exports.getAdminById = (req, res) => {
    let adminId = req.params.id;

    Admin.findOne({
        _id: adminId
    }).then(admin => {
        return res.status(200).send({ admin }).end();
    }).catch(error => {
        return res.status(500).send({ message: error }).end();
    })
};

exports.updateProfile = (req, res) => {
    let schema = Joi.object({
        email: Joi.string().required(),
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        phoneNo: Joi.string().required(),
        address: Joi.string().required()
    });
    let payload = req.body;
    let test = schema.validate(payload);
    if (test.error) {
        return res.status(400).send({ message: test.error.details[0].message });
    }
    Admin.findOneAndUpdate(
        {
            _id: req.params.id
        },
        {
            $set: {
                email: payload.email,
                firstName: payload.firstName,
                lastName: payload.lastName,
                phoneNo: payload.phoneNo,
                address: payload.address
            }
        }
    ).then(updated => {
        return res.status(200).send({ message: "Profile Updated", admin: updated }).end();
    }).catch(error => {
        return res.status(500).send({ message: error }).end();
    });
};

function encode(password) {
    return crypto.createHash('sha256').update(password).digest('hex');
}
function encrypt(hexString) {
    let salt = bcrypt.genSaltSync(12);
    return bcrypt.hashSync(hexString, salt);
}
function isSame(hexString, hashString) {
    return bcrypt.compareSync(hexString, hashString);
}
