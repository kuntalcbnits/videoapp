const debug = require('debug')('utaxiu-api:email.controller')
const EmailTemplate = require("../../models/emailtemplate.model");
const Joi = require("@hapi/joi");

exports.createEmailTemplate = (req, res) => {
    const schema = Joi.object({
        subject: Joi.string().required(),
        content: Joi.string().required(),
        status: Joi.boolean().required()
    });
    const result = schema.validate(req.body);
    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    } else {
        const emailTemplate = new EmailTemplate({
            subject: req.body.subject,
            content: req.body.content,
            status: req.body.status
        });
        emailTemplate.save()
            .then(() => {
                res.status(201).send({ message: "Email template created successfully." });
            })
            .catch(e => {
                res.status(400).send({ message: e.message });
            });
    }
}

exports.updateEmailTemplate = (req, res) => {
    let emailId = req.params.id
    const schema = Joi.object({
        subject: Joi.string(),
        content: Joi.string(),
        status: Joi.boolean().required(),
        isDeleted: Joi.boolean().required()
    })
    const result = schema.validate(req.body);
    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    } else {
        EmailTemplate.find({
            _id: emailId,
            isDeleted: false
        }).then(result => {
            if (result.length != 0) {
                EmailTemplate.findOneAndUpdate({
                    _id: emailId
                }, {
                        "$set": {
                            subject: req.body.subject,
                            content: req.body.content,
                            status: req.body.status,
                            isDeleted: req.body.isDeleted
                        }
                    }).then(result => {
                        return res.status(200).send({ message: 'Email template updated successfully.' });
                    }).catch(err => {
                        return res.status(500).send({ message: err.message });
                    });
            } else {
                return res.status(404).send({ message: 'Email template with this ID not found.' });
            }
        }).catch(err => {
            return res.status(500).send({ message: err.message });
        })
    }
}

exports.getAllEmailTemplates = (req, res) => {
    EmailTemplate.find({
        isDeleted: false
    }).then(results => {
        
        return res.status(200).send({ message: "EmailTemplate details found", EmailTemplateList: results });
    }).catch(err => {
        return res.status(400).send({ message: err.message });
    });
}

exports.getEmailTemplateById = (req, res) => {
    EmailTemplate.findOne({
        _id: req.params.id,
        isDeleted: false
    }).then(result => {
        return res.status(200).send({ message: `Email Template ID ${result._id}`, EmailTemplate: result});
    }).catch(err => {
        return res.status(500).send({ message: err.message });
    });
};

exports.deleteEmailTemplate = (req, res) => {
    let emailId = req.params.id;
    EmailTemplate.findOne({
        _id: emailId,
        isDeleted: false
    }).then(result => {
        if(!result) {
            return res.status(404).send({ message: "Email Template Not Found." }).end();
        }
        EmailTemplate.findOneAndUpdate(
            {
                _id: emailId
            },
            {
                $set: {
                    isDeleted: true
                }
            }
        ).then(() => {
            return res.status(200).send({ message: "Email Template updated successfully." });
        }).catch(err => {
            return res.status(400).send({ message: err.message });
        });
    })
}
