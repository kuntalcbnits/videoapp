const PaymentHistory = require('../../models/paymenthistory.model');
const Joi = require("@hapi/joi");
const mongoose = require('mongoose');

exports.getAllPaymentHistory = (req, res) => {
    PaymentHistory
        .aggregate([
            {
                $lookup: {
                    from: 'drivers',
                    localField: 'driverId',
                    foreignField: '_id',
                    as: 'driverDetails'
                }
            },
            {
                $group: {
                    _id: '$driverId',
                    driverInfo: { '$first': '$driverDetails' },
                    total: {
                        $sum: '$amount'
                    }
                }
            },
            {
                $unwind: '$driverInfo'
            }
        ])
        .sort({ date: -1 })
        .then(result => {
            return res.status(200).send({ PaymentHistory: result }).end();
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
};

exports.addPaymentHistory = (req, res) => {
    const scheme = Joi.object({
        driverId: Joi.string().required(),
        rideId: Joi.string().required(),
        amount: Joi.number().required(),
    });
    const test = scheme.validate(req.body);
    if (test.error) {
        return res.status(400).send({ message: test.error.details[0].message });
    }
    const newPayment = new PaymentHistory({
        driverId: req.body.driverId,
        rideId: req.body.rideId,
        amount: req.body.amount
    });
    newPayment
        .save()
        .then(() => {
            return res.status(200).send({ message: 'Payment history added' }).end();
        })
        .catch(error => {
            debug(error);
            return res.status(500).send({ message: error }).end();
        })
}

exports.getPaymentHistoryByDriver = (req, res) => {
    let driverId = req.params.id;

    PaymentHistory
        .aggregate([
            {
                $match: {
                    driverId: mongoose.Types.ObjectId(driverId)
                }
            },
            {
                $lookup: {
                    from: 'drivers',
                    localField: 'driverId',
                    foreignField: '_id',
                    as: 'driverInfo'
                }
            },
            {
                $lookup: {
                    from: 'ridehistories',
                    localField: 'rideId',
                    foreignField: '_id',
                    as: 'rideInfo'
                }
            },
            {
                $unwind: "$driverInfo"
            },
            {
                $unwind: "$rideInfo"
            }
        ])
        .sort({ date: -1 })
        .then(result => {
            return res.status(200).send({ PaymentHistory: result }).end();
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
}
