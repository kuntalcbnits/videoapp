const debug = require('debug')('utaxiu-api:vehicle.controller');
const Vehicle = require('../../models/vehicle.model');
const Joi = require("@hapi/joi");
const mongoose = require('mongoose');

exports.getAllVehicles = (req, res) => {
    Vehicle
        .aggregate([
            {
                $match: {
                    isDeleted: false
                }
            },
            {
                $lookup: {
                    from: 'vehicletypes',
                    localField: 'vehicleTypeId',
                    foreignField: '_id',
                    as: 'vehicleType'
                }
            },
            {
                $unwind: '$vehicleType'
            }
        ])
        .then(Vehicles => {
            return res.status(200).send({ message: "Vehicles Found", Vehicles }).end();
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
}

exports.getVehicleById = (req, res) => {
    let vehicleId = req.params.id;

    Vehicle
        .aggregate([
            {
                $match: {
                    _id: mongoose.Types.ObjectId(vehicleId)
                }
            },
            {
                $lookup: {
                    from: 'vehicletypes',
                    localField: 'vehicleTypeId',
                    foreignField: '_id',
                    as: 'vehicleType'
                }
            },
            {
                $unwind: '$vehicleType'
            }
        ])
        .then(Vehicle => {
            return res.status(200).send({ message: "Vehicle Found", Vehicle }).end();
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
}

exports.createVehicle = (req, res) => {
    let schema = Joi.object({
        vehicleTypeId: Joi.string().required(),
        registrationNo: Joi.string().required(),
        manufactureYear: Joi.string().required(),
        state: Joi.string().required(),
        color: Joi.string().required(),
        status: Joi.boolean().required()
    });

    let test = schema.validate(req.body);
    if (test.error) {
        return res.status(400).send({ message: test.error.details[0].message }).end();
    }

    let newVehicle = new Vehicle({
        vehicleTypeId: req.body.vehicleTypeId,
        registrationNo: req.body.registrationNo,
        manufactureYear: req.body.manufactureYear,
        state: req.body.state,
        color: req.body.color,
        status: req.body.status
    })

    newVehicle
        .save()
        .then(() => {
            return res.status(200).send({ message: "Vehicle added" }).end();
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
}

exports.updateVehicle = (req, res) => {
    let vehicleId = req.params.id;

    let schema = Joi.object({
        vehicleTypeId: Joi.string().required(),
        registrationNo: Joi.string().required(),
        manufactureYear: Joi.string().required(),
        state: Joi.string().required(),
        color: Joi.string().required(),
        status: Joi.boolean().required(),
        isDeleted: Joi.boolean().required()
    });
    debug(req.body);
    let test = schema.validate(req.body);
    if (test.error) {
        return res.status(400).send({ message: test.error.details[0].message }).end();
    }

    Vehicle
        .findOne({
            _id: vehicleId,
            isDeleted: false
        })
        .then(result => {
            if (!result) {
                return res.status(404).send({ message: "Vehicle not found" }).end();
            }
            Vehicle
                .findOneAndUpdate(
                    {
                        _id: vehicleId
                    },
                    {
                        $set: {
                            vehicleTypeId: req.body.vehicleTypeId,
                            registrationNo: req.body.registrationNo,
                            manufactureYear: req.body.manufactureYear,
                            state: req.body.state,
                            color: req.body.color,
                            status: req.body.status,
                            isDeleted: req.body.isDeleted
                        }
                    }
                )
                .then(updated => {
                    return res.status(200).send({ message: "Vehicle updated", Vehicle: updated }).end();
                })
                .catch(error => {
                    return res.status(500).send({ message: error }).end();
                })
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
}

exports.deleteVehicle = (req, res) => {
    let vehicleId = req.params.id;

    Vehicle
        .findOne({
            _id: vehicleId,
            isDeleted: false
        })
        .then(result => {
            if (!result) {
                return res.status(404).send({ message: "Vehicle not found" }).end();
            }
            Vehicle
                .findOneAndUpdate(
                    {
                        _id: vehicleId
                    },
                    {
                        $set: {
                            isDeleted: true
                        }
                    }
                )
                .then(updated => {
                    return res.status(200).send({ message: "Vehicle deleted" }).end();
                })
                .catch(error => {
                    return res.status(500).send({ message: error }).end();
                })
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
}
