const User = require('../../models/user.model');
const crypto = require('crypto');
const Joi = require("@hapi/joi");
const bcrypt = require('bcryptjs');

const config = require('../../config/config');

// Inserting a user data into database during signup
exports.createUser = function (req, res) {
    // console.log(req.file);
    const schema = Joi.object({
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        phoneNo: Joi.string().required(),
        email: Joi.string().required(),
        address: Joi.string().required(),
        // password: Joi.string().required(),
        status: Joi.boolean().required()
    });
    const payload = req.body;
    const result = schema.validate(payload);
    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    } else {
        // let encoded = encode(payload.email, payload.password);
        // let encrypted = encrypt(encoded);
        const newUser = new User({
            firstName: payload.firstName,
            lastName: payload.lastName,
            phoneNo: payload.phoneNo,
            email: payload.email,
            address: payload.address,
            // password: payload.password,
            personalID: config.userImageDBPath + req.files["personalID"][0].filename,
            // drivingLicence: config.driverImageDBPath + req.files["drivingLicence"][0].filename,
            status: payload.status,
        });
        newUser.save().then(() => {
            return res.status(201).send({ message: "User added successfully." });
        }).catch(err => {
            return res.status(500).send({ message: err.message });
        });
    }
};

exports.updateUser = (req, res) => {
    const userId = req.params.id;
    const schema = Joi.object({
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        phoneNo: Joi.string().required(),
        email: Joi.string().required(),
        address: Joi.string().required(),
        personalID: Joi.string().required(),
        status: Joi.boolean().required(),
        isDeleted: Joi.boolean().required()
    });
    const payload = req.body;
    const result = schema.validate(payload);
    // console.log(result);
    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    } else {
        User.find({
            _id: userId,
            isDeleted: false
        }).then(result => {
            if (result) {
                let updateObj = {
                    firstName: payload.firstName,
                    lastName: payload.lastName,
                    phoneNo: payload.phoneNo,
                    email: payload.email,
                    address: payload.address,
                    // password: payload.password,
                    personalID: req.files["personalID"] ?  config.userImageDBPath + req.files["personalID"][0].filename : payload.personalID,
                    // drivingLicence: config.driverImageDBPath + req.files["drivingLicence"][0].filename,
                    status: payload.status,
                    isDeleted: payload.isDeleted
                }
                User.findOneAndUpdate(
                    {
                        _id: userId
                    },
                    {
                        $set: updateObj
                    }
                ).then(() => {
                    return res.status(200).send({ message: "User updated successfully." });
                }).catch(err => {
                    return res.status(400).send({ message: err.message });
                });
            } else {
                return res.status(404).send({ message: "User Not Found." });
            }
        }).catch(err => {
            return res.status(500).send({ message: err.message });
        });
    }
};

exports.getAllUsers = (req, res) => {
    User.find({
        isDeleted: false
    }).then(results => {
        return res.status(200).send({ message: "User details found", Users: results });
    }).catch(err => {
        return res.status(400).send({ message: err.message });
    });
};

exports.getUserById = (req, res) => {
    User.findOne({
        _id: req.params.id,
        isDeleted: false
    }).then(result => {
        if (!result) {
            return res.status(404).send({ message: "User Not Found." });
        }
        return res.status(200).send({ message: `User ID ${result._id}`, User: result });
    }).catch(err => {
        return res.status(500).send({ message: err.message });
    });
};

exports.deleteUser = (req, res) => {
    let userId = req.params.id;
    User.findOne({
        _id: userId,
        isDeleted: false
    }).then(result => {
        if (!result) {
            return res.status(404).send({ message: "Driver No Found." }).end();
        }
        User.findOneAndUpdate(
            {
                _id: userId
            },
            {
                $set: {
                    isDeleted: true
                }
            }
        ).then(() => {
            return res.status(200).send({ message: "Driver updated successfully." });
        }).catch(err => {
            return res.status(400).send({ message: err.message });
        });
    })
}

function encode(email, password) {
    let emailHash = crypto.createHash('sha256').update(email).digest('hex');
    let pwdHash = crypto.createHash('sha256').update(password).digest('hex');

    return [emailHash, pwdHash].join('$');
}

function encrypt(hashString) {
    let salt = bcrypt.genSaltSync(16);

    return bcrypt.hashSync(hashString, salt);
}
