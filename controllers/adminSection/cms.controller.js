const debug = require('debug')('utaxiu-api:cms.controller');
const CMS = require("../../models/cms.model");
const Joi = require("@hapi/joi");

exports.createCMS = (req, res, next) => {
    const schema = Joi.object({
        heading: Joi.string().required(),
        content: Joi.string().required(),
        pageName: Joi.string().required(),
        status: Joi.boolean().required()
    });
    const result = schema.validate(req.body);
    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    } else {
        const newCMS = new CMS({
            heading: req.body.heading,
            content: req.body.content,
            pageName: req.body.pageName,
            status: req.body.status
        });
        newCMS
            .save()
            .then(() => {
                res.status(200).send({ message: "CMS added successfully." });
            })
            .catch(err => {
                res.status(500).send({ message: err.message });
            });
    }
};

exports.updateCMS = (req, res, next) => {
    let cmsId = req.params.id;
    const schema = Joi.object({
        heading: Joi.string(),
        content: Joi.string(),
        pageName: Joi.string(),
        status: Joi.boolean().required(),
        isDeleted: Joi.boolean().required()
    });
    const result = schema.validate(req.body);
    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    } else {
        CMS.find({
            _id: cmsId,
            isDeleted: false
        }).then(result => {
            if (result.length != 0) {
                CMS.findOneAndUpdate(
                    {
                        _id: cmsId
                    },
                    {
                        $set: {
                            heading: req.body.heading,
                            content: req.body.content,
                            pageName: req.body.pageName,
                            status: req.body.status,
                            isDeleted: req.body.isDeleted
                        }
                    }
                ).then(() => {
                    return res.status(200).send({ message: "CMS updated successfully." });
                }).catch(err => {
                    return res.status(400).send("Error => " + err);
                });
            } else {
                return res.status(404).send({ message: "CMS Not Found." });
            }
        }).catch(err => {
            return res.status(500).send({ message: err.message });
        });
    }
};

exports.getAllCMS = (req, res, next) => {
    CMS.find({
        isDeleted: false
    }).then(results => {
        
        return res.status(200).send({ message: "CMS details found", CMSList: results });
    }).catch(err => {
        return res.status(400).send({ message: err.message });
    });
};

exports.getCMSById = (req, res) => {
    CMS.findOne({
        _id: req.params.id,
        isDeleted: false
    }).then(result => {
        if (!result) {
            return res.status(404).send({ message: "CMS Not Fount" });
        }
        return res.status(200).send({ message: `CMS ID ${result._id}`, CMS: result });
    }).catch(err => {
        return res.status(500).send({ message: err.message });
    });
};

exports.deleteCMS = (req, res) => {
    let cmsId = req.params.id;
    CMS.findOne({
        _id: cmsId,
        isDeleted: false
    }).then(result => {
        if(!result) {
            return res.status(404).send({ message: "CMS No Found." }).end();
        }
        CMS.findOneAndUpdate(
            {
                _id: cmsId
            },
            {
                $set: {
                    isDeleted: true
                }
            }
        ).then(() => {
            return res.status(200).send({ message: "CMS updated successfully." });
        }).catch(err => {
            return res.status(400).send({ message: err.message });
        });
    })
}
