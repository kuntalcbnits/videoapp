const Driver = require('../../models/driver.model');
const multer = require('multer');
const upload = multer();
const crypto = require('crypto');
const Joi = require("@hapi/joi");
const bcrypt = require('bcryptjs');

const config = require('../../config/config');

// Inserting a user data into database during signup
exports.createDriver = function (req, res) {
    const schema = Joi.object({
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        phoneNo: Joi.string().required(),
        email: Joi.string().required(),
        address: Joi.string().required(),
        password: Joi.string().required(),
        status: Joi.boolean().required()
    });
    const payload = req.body;
    const result = schema.validate(payload);
    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    } else {
        let encoded = encode(payload.password);
        let encrypted = encrypt(encoded);
        const newDriver = new Driver({
            firstName: payload.firstName,
            lastName: payload.lastName,
            phoneNo: payload.phoneNo,
            email: payload.email,
            address: payload.address,
            password: encrypted,
            personalID: req.files["personalID"] ? config.driverImageDBPath + req.files["personalID"][0].filename : '',
            drivingLicence: req.files["drivingLicence"] ? config.driverImageDBPath + req.files["drivingLicence"][0].filename : '',
            status: payload.status,
        });
        newDriver.save().then(() => {
            return res.status(201).send({ message: "Driver added successfully." });
        }).catch(err => {
            return res.status(500).send({ message: err.message });
        });
    }
};

exports.updateDriver = (req, res) => {
    const driverId = req.params.id;
    console.log(req.body);
    const schema = Joi.object({
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        phoneNo: Joi.string().required(),
        email: Joi.string().required(),
        address: Joi.string().required(),
        // details: Joi.string().required(),
        // password: Joi.string().required(),
        dateOfBirth: Joi.date().required(),
        gender: Joi.string().required(),
        educationalQualification: Joi.string().required(),
        personalIDNo: Joi.string().required(),
        drivingLicenceNo: Joi.string().required(),
        // personalID: Joi.string().required(),
        // drivingLicence: Joi.string().required(),
        status: Joi.boolean().required(),
        isDeleted: Joi.boolean().required()
    });
    const payload = req.body;
    const result = schema.validate(payload);
    if (result.error) {
        return res.status(400).send({ message: result.error.details[0].message });
    } else {
        Driver.findOne({
            _id: driverId,
            isDeleted: false
        }).then(result => {
            if (result) {
                let updateObj = {}
                if (Object.keys(req.files).length > 0) {
                    // console.log('files present');
                    updateObj = {
                        firstName: payload.firstName,
                        lastName: payload.lastName,
                        phoneNo: payload.phoneNo,
                        email: payload.email,
                        address: payload.address,
                        // details: payload.details,
                        // password: payload.,
                        dateOfBirth: payload.dateOfBirth,
                        gender: payload.gender,
                        educationalQualification: payload.educationalQualification,
                        personalIDNo: payload.personalIDNo,
                        drivingLicenceNo: payload.drivingLicenceNo,
                        personalID: req.files["personalID"] ? config.driverImageDBPath + req.files["personalID"][0].filename : result.personalID,
                        drivingLicence: req.files["drivingLicence"] ? config.driverImageDBPath + req.files["drivingLicence"][0].filename : result.drivingLicence,
                        status: payload.status,
                        isDeleted: payload.isDeleted
                    }
                } else {
                    // console.log('files absent');
                    updateObj = {
                        firstName: payload.firstName,
                        lastName: payload.lastName,
                        phoneNo: payload.phoneNo,
                        email: payload.email,
                        address: payload.address,
                        // details: payload.details,
                        // password: payload.,
                        dateOfBirth: payload.dateOfBirth,
                        gender: payload.gender,
                        educationalQualification: payload.educationalQualification,
                        personalIDNo: payload.personalIDNo,
                        drivingLicenceNo: payload.drivingLicenceNo,
                        status: payload.status,
                        isDeleted: payload.isDeleted
                    }
                }
                Driver.findOneAndUpdate(
                    {
                        _id: driverId
                    },
                    {
                        $set: updateObj
                    }
                ).then(() => {
                    return res.status(200).send({ message: "Driver updated successfully." });
                }).catch(err => {
                    return res.status(400).send({ message: err.message });
                });
            } else {
                return res.status(404).send({ message: "CMS Not Found." });
            }
        }).catch(err => {
            return res.status(500).send({ message: err.message });
        });
    }
};

exports.getAllDrivers = (req, res) => {
    Driver.find({
        isDeleted: false
    }).then(results => {
        return res.status(200).send({ message: "Driver details found", Drivers: results });
    }).catch(err => {
        return res.status(400).send({ message: err.message });
    });
};

exports.getDriverById = (req, res) => {
    Driver.findOne({
        _id: req.params.id,
        isDeleted: false
    }).then(result => {
        if (!result) {
            return res.status(404).send({ message: "Driver Not Found." });
        }
        return res.status(200).send({ message: `Driver ID ${result._id}`, Driver: result });
    }).catch(err => {
        return res.status(500).send({ message: err.message });
    });
};

exports.deleteDriver = (req, res) => {
    let driverId = req.params.id;
    Driver.findOne({
        _id: driverId,
        isDeleted: false
    }).then(result => {
        if(!result) {
            return res.status(404).send({ message: "Driver No Found." }).end();
        }
        Driver.findOneAndUpdate(
            {
                _id: driverId
            },
            {
                $set: {
                    isDeleted: true
                }
            }
        ).then(() => {
            return res.status(200).send({ message: "Driver updated successfully." });
        }).catch(err => {
            return res.status(400).send({ message: err.message });
        });
    })
}

function encode(password) {
    return crypto.createHash('sha256').update(password).digest('hex');
}

function encrypt(hashString) {
    let salt = bcrypt.genSaltSync(16);

    return bcrypt.hashSync(hashString, salt);
}
