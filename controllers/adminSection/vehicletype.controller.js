const debug = require('debug')('utaxiu-api:vehicletype.controller')
const VehicleType = require('../../models/vehicletype.model');
const Joi = require("@hapi/joi");

exports.getAllVehicleTypes = (req, res) => {
    VehicleType
        .find({
            isDeleted: false
        })
        .then(result => {
            return res.status(200).send({ message: "Vehicle Types Found", VehicleTypes: result }).end();
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
}

exports.getVehicleTypeById = (req, res) => {
    let vehicleTypeId = req.params.id;
    VehicleType
        .findOne({
            _id: vehicleTypeId
        })
        .then(result => {
            return res.status(200).send({ message: "Vehicle Type Found", VehicleType: result }).end();
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
}

exports.createVehicleType = (req, res) => {
    let schema = Joi.object({
        name: Joi.string().required(),
        baseFare: Joi.string().required(),
        status: Joi.boolean().required()
    })
    let test = schema.validate(req.body);
    if (test.error) {
        return res.status(400).send({ message: test.error.details[0].message });
    }

    let newVehicleType = new VehicleType({
        name: req.body.name,
        baseFare: req.body.baseFare,
        status: req.body.status
    });

    newVehicleType
        .save()
        .then(() => {
            return res.status(200).send({ message: "Vehicle Type Added" }).end();
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
}

exports.updateVehicleType = (req, res) => {
    let vehicleTypeId = req.params.id;

    let schema = Joi.object({
        name: Joi.string().required(),
        baseFare: Joi.string().required(),
        status: Joi.boolean().required(),
        isDeleted: Joi.boolean().required()
    })
    let test = schema.validate(req.body);
    if (test.error) {
        return res.status(400).send({ message: test.error.details[0].message });
    }

    VehicleType
        .findOne({
            _id: vehicleTypeId,
            isDeleted: false
        })
        .then(result => {
            if (!result) {
                return res.status(404).send({ message: "Vehicle type not found" }).end();
            }
            VehicleType
                .findOneAndUpdate(
                    {
                        _id: vehicleTypeId
                    },
                    {
                        $set: {
                            name: req.body.name,
                            baseFare: req.body.baseFare,
                            status: req.body.status,
                            isDeleted: req.body.isDeleted
                        }
                    }
                )
                .then(updated => {
                    return res.status(200).send({ message: "Vehicle Type Updated", VehicleType: updated }).end();
                })
                .catch(error => {
                    return res.status(400).send({ message: error });
                })
        })
        .catch(error => {
            return res.status(400).send({ message: error });
        })
};

exports.deleteVehicleType = (req, res) => {
    let vehicleTypeId = req.params.id;
    VehicleType
        .findOne({
            _id: vehicleTypeId,
            isDeleted: false
        })
        .then(result => {
            if (!result) {
                return res.status(404).send({ message: "Vehicle type not found" }).end();
            }
            VehicleType
                .findOneAndUpdate(
                    {
                        _id: vehicleTypeId
                    },
                    {
                        $set: {
                            isDeleted: true
                        }
                    }
                )
                .then(updated => {
                    return res.status(200).send({ message: "Vehicle Type Deleted" }).end();
                })
                .catch(error => {
                    return res.status(400).send({ message: error });
                })
        })
        .catch(error => {
            return res.status(400).send({ message: error });
        })
}
