const debug = require('debug')('utaxiu-api:ridehistory.controller');
const RideHistory = require('../../models/ridehistory.model');
const Joi = require("@hapi/joi");
const mongoose = require('mongoose');

exports.getAllRideHistory = (req, res) => {
    RideHistory
        .aggregate([
            {
                $lookup: {
                    from: 'drivers',
                    localField: 'driverId',
                    foreignField: '_id',
                    as: 'driverDetails'
                }
            },
            {
                $group: {
                    _id: '$driverId',
                    driverInfo: { '$first': '$driverDetails' },
                    count: {
                        $sum: 1
                    }
                }
            },
            {
                $unwind: '$driverInfo'
            }
        ])
        .sort({ date: -1 })
        .then(result => {
            return res.status(200).send({ RideHistory: result }).end();
        })
        .catch(error => {
            debug(error)
            return res.status(500).send({ message: error }).end();
        })
};

exports.addRideHistory = (req, res) => {
    const scheme = Joi.object({
        driverId: Joi.string().required(),
        passengerId: Joi.string().required(),
        amount: Joi.number().required(),
        route: Joi.object().required(),
        distance: Joi.string().required(),
        time: Joi.string().required()
    });
    const test = scheme.validate(req.body);
    if (test.error) {
        return res.status(400).send({ message: test.error.details[0].message });
    }
    const newRideHistory = new RideHistory({
        driverId: req.body.driverId,
        passengerId: req.body.passengerId,
        amount: req.body.amount,
        route: req.body.route,
        distance: req.body.distance,
        time: req.body.time
    });
    newRideHistory
        .save()
        .then(() => {
            return res.status(200).send({ message: 'Ride history added' }).end();
        })
        .catch(error => {
            debug(error);
            return res.status(500).send({ message: error }).end();
        })
}

exports.getRideHistoryByDriver = (req, res) => {
    let driverId = req.params.id;
    let date = req.query.date;
    // console.log(date);

    RideHistory
        .aggregate([
            {
                $match: {
                    driverId: mongoose.Types.ObjectId(driverId)
                }
            },
            {
                $lookup: {
                    from: 'drivers',
                    localField: 'driverId',
                    foreignField: '_id',
                    as: 'driverInfo'
                }
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'passengerId',
                    foreignField: '_id',
                    as: 'userInfo'
                }
            },
            {
                $unwind: "$driverInfo"
            },
            {
                $unwind: "$userInfo"
            }
        ])
        .sort({ date: -1 })
        .then(result => {
            return res.status(200).send({ RideHistory: result }).end();
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
}
