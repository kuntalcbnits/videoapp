const debug        = require('debug')('utaxiu-api:subscription.controller'),
      Joi          = require('@hapi/joi'),
      Subscription = require('../../models/subscription.model');

exports.getAllSubscription = (req, res) => {
    Subscription
        .find({
            isDeleted: false
        })
        .then(Subscriptions => {
            return res.status(200).send({ message: "Subscriptions found", Subscriptions }).end();
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
};

exports.getSubscriptionById = (req, res) => {
    let subscriptionId = req.params.id;
    Subscription
        .findOne({
            _id: subscriptionId,
            isDeleted: false
        })
        .then(Subscription => {
            return res.status(200).send({ message: "Subscription found", Subscription }).end();
        })
        .catch(error => {
            return res.status(500).send({ message: error }).end();
        })
};

exports.createSubscription = (req, res) => {
    let schema = Joi.object({
        name: Joi.string().required(),
        baseTripNo: Joi.number().required(),
        baseAmountReceivable: Joi.number().required(),
        bonusTripNo: Joi.number().required(),
        bonusAmountReceivable: Joi.number().required(),
        status: Joi.boolean().required()
    });
    let test = schema.validate(req.body);
    if (test.error) {
        return res.status(400).send({ message: test.error.details[0].message });
    }

    let newSubscription = new Subscription({
        name: req.body.name,
        baseTripNo: req.body.baseTripNo,
        baseAmountReceivable: req.body.baseAmountReceivable,
        bonusTripNo: req.body.bonusTripNo,
        bonusAmountReceivable: req.body.bonusAmountReceivable,
        status: req.body.status
    })

    newSubscription
        .save()
        .then(() => {
            return res.status(200).send({ message: "Subscription created" }).end();
        })
        .catch(error => {
            return res.status(500).send({ message: error });
        })
}

exports.updateSubscription = (req, res) => {
    let subscriptionId = req.params.id;
    let schema = Joi.object({
        name: Joi.string().required(),
        baseTripNo: Joi.number().required(),
        baseAmountReceivable: Joi.number().required(),
        bonusTripNo: Joi.number().required(),
        bonusAmountReceivable: Joi.number().required(),
        status: Joi.boolean().required(),
        isDeleted: Joi.boolean().required()
    });
    let test = schema.validate(req.body);
    if (test.error) {
        return res.status(400).send({ message: test.error.details[0].message });
    }

    Subscription
        .findOne({
            _id: subscriptionId,
            isDeleted: false
        })
        .then(result => {
            if (!result) {
                return res.status(404).send({ message: "Subscription not found" }).end();
            }
            Subscription
                .findOneAndUpdate(
                    {
                        _id: subscriptionId
                    },
                    {
                        $set: {
                            name: req.body.name,
                            baseTripNo: req.body.baseTripNo,
                            baseAmountReceivable: req.body.baseAmountReceivable,
                            bonusTripNo: req.body.bonusTripNo,
                            bonusAmountReceivable: req.body.bonusAmountReceivable,
                            status: req.body.status
                        }
                    }
                )
                .then(updated => {
                    return res.status(200).send({ message: "Subscription Updated", Subscription: updated }).end();
                })
                .catch(error => {
                    return res.status(400).send({ message: error });
                })
        })
        .catch(error => {
            return res.status(400).send({ message: error });
        });
}
