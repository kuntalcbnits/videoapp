const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let DriverSchema = new Schema({
    firstName: {
        type: String,
        // required: true
    },
    lastName: {
        type: String,
        // required: true
    },
    phoneNo: {
        type: String,
        // required: true
    },
    mobile: {
        type: String,
        // required: true
    },
    email: {
        type: String,
        // required: true
    },
    password: {
        type: String,
        // required: true
    },
    address: {
        type: String
    },
    details: {
        type: String,
        // required: true
    },
    dateOfBirth: {
        type: Date
    },
    gender: {
        type: String,
        enum: ["M", "F", "O"]
    },
    educationalQualification: {
        type: String
    },
    personalIDNo: {
        type: String
    },
    personalID: {
        type: String,
        // required: true
    },
    drivingLicenceNo: {
        type: String
    },
    drivingLicence: {
        type: String,
        // required: true
    },
    profileImg: {
        type: String
    },
    status: {
        type: Boolean,
        // required: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    date: {
        type: Date,
        default: Date.now()
    }
 });



 DriverSchema.set('toJSON', {
    transform: function(doc, ret, opt) {
        delete ret['password']
        return ret
    }
})

 // Export the model
module.exports = mongoose.model('Driver', DriverSchema);
