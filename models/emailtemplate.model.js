const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EmailTemplate = new Schema({
    subject: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    status: {
        type: Boolean,
        required: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    date: {
        type: Date,
        default: Date.now()
    }
});


module.exports = mongoose.model('EmailTemplate', EmailTemplate);
