const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Cms=new Schema({
    heading:{
        type:String,
        required:true
    },
    pageName:{
        type:String,
        required:true
    },
    content:{
        type: String,
        required:true
    },
    status:{
        type:Boolean,
        required:true
    },
    isDeleted:{
        type:Boolean,
        default:false
    },
    date: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model('CMS',Cms);
