const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let AdminSchema = new Schema({
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    phoneNo: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    role: {
        type: String,
        enum: ["superAdmin","subAdmin"],
        default: "subAdmin"
    },
    date: {
        type: Date,
        default: Date.now()
    },
    resetPasswordToken: {
        type: String
    },
    tokenExpiration: {
        type: Number
    }
});

AdminSchema.set('toJSON', {
    transform: function(doc, ret, opt) {
        delete ret['password']
        return ret
    }
})

module.exports = mongoose.model('Admin',AdminSchema);
