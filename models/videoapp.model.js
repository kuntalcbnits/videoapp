const mongoose = require('mongoose');
const Schema = mongoose.Schema;


let VideoAppSchema = new Schema({
    roomId:{
        type: String,
    },
    roomName:{
        type: String,
    },
    roomTitle:{
        type: String,
    },
    roomKey:{
        type: String,
    },
    roomStatus:{
        type: Boolean,
    },
    createDate:{
        type: Date,
    }
 });



 VideoAppSchema.set('toJSON', {
    transform: function(doc, ret, opt) {
        delete ret['roomKey']
        return ret
    }
})

 // Export the model
module.exports = mongoose.model('VideoRoom', VideoAppSchema);
