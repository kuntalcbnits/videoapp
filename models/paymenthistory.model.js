const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let PaymentHistorySchema = new Schema({
    driverId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Driver'
    },
    rideId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'RideHistory'
    },
    amount: {
        type: Number
    },
    date: {
        type: Date,
        default: Date.now()
    }
});


// Export the model
module.exports = mongoose.model('PaymentHistory', PaymentHistorySchema);
