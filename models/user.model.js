const mongoose = require('mongoose');
const Schema = mongoose.Schema;


let UserSchema = new Schema({
    fullName: {
        type: String,
    },
    phoneNo: {
        type: String,
    },
    emailId: {
        type: String,
    },
    password: {
        type: String,
    },
     createDate: {
        type: Date,
    },
    deviceIMEI: {
        type: String,
    },
    deviceHash: {
        type: String,
    },
    status: {
        type: Boolean,
    },
    isDeleted: {
        type: Boolean,
    },
    dateOfBirth: {
        type: Date
    },
    gender: {
        type: String,
    },
    address: {
        type: String
    },
    about: {
        type: String,
    },
    educationalQualification: {
        type: String
    },
    paymentMethod: {
        type: String,
    },
    userOTP: {
        type: String,
    }
 });




// let UserSchema = new Schema({
//     firstName: {
//         type: String,
//         // required: true
//     },
//     lastName: {
//         type: String,
//         // required: true
//     },
//     phoneNo: {
//         type: String,
//         // required: true
//     },
//     mobile: {
//         type: String,
//         // required: true
//     },
//     email: {
//         type: String,
//         // required: true
//     },
//     password: {
//         type: String,
//         // required: true
//     },
//     address: {
//         type: String
//     },
//     details: {
//         type: String,
//         // required: true
//     },
//     dateOfBirth: {
//         type: Date
//     },
//     gender: {
//         type: String,
//         enum: ["M", "F", "O"]
//     },
//     educationalQualification: {
//         type: String
//     },
//     personalIDNo: {
//         type: String
//     },
//     personalID: {
//         type: String,
//         // required: true
//     },
//     status: {
//         type: Boolean,
//         // required: true
//     },
//     isDeleted: {
//         type: Boolean,
//         default: false
//     },
//     date: {
//         type: Date,
//         default: Date.now()
//     }
//  });



 UserSchema.set('toJSON', {
    transform: function(doc, ret, opt) {
        delete ret['password']
        return ret
    }
})

 // Export the model
module.exports = mongoose.model('User', UserSchema);
