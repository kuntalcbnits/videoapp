const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let VehicleTypeSchema = new Schema({
    name: {
        type: String
    },
    baseFare: {
        type: String
    },
    status: {
        type: Boolean,
        // required: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    date: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model('VehicleType', VehicleTypeSchema);
