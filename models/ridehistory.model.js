const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let RideHistorySchema = new Schema({
    driverId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Driver'
    },
    passengerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    route: {
        type: Object
    },
    amount: {
        type: Number
    },
    paymentType: {
        type: String
    },
    distance: {
        type: String
    },
    time: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now()
    }
});


// Export the model
module.exports = mongoose.model('RideHistory', RideHistorySchema);
