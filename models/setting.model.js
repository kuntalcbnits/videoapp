const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Setting=new Schema({
    paypalEmail:{
        type:String,
        required:true
    },
    siteEmail:{
        type:String,
        required:true
    },
    mobile:{
        type: String,
        required:true
    },
    skype:{
        type:String,
        required:true
    },
    siteName:{
        type:String,
        required:true
    },
    address:{
        type:String,
        required:true
    },
    iconFile: {
        type: String
    },
    logoFile: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model('Setting',Setting);
