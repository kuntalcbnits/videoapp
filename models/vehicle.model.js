const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let VehicleSchema = new Schema({
    vehicleTypeId: {
        type: mongoose.Schema.Types.ObjectId
    },
    registrationNo: {
        type: String
    },
    manufactureYear: {
        type: String
    },
    state: {
        type: String
    },
    color: {
        type: String
    },
    status: {
        type: Boolean,
        // required: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    date: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model('Vehicle', VehicleSchema);
