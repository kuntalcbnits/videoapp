const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let SubscriptionSchema = new Schema({
    name: {
        type: String
    },
    baseTripNo: {
        type: Number
    },
    baseAmountReceivable: {
        type: Number
    },
    bonusTripNo: {
        type: Number
    },
    bonusAmountReceivable: {
        type: Number
    },
    status: {
        type: Boolean
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    date: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model('Subscription', SubscriptionSchema);
