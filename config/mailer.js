const debug = require('debug')('utaxiu-api:mailer');
const nodemailer = require('nodemailer');
const format = require('string-template');
const config = require('../config/config');

const Email = require('../models/emailtemplate.model');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    // port: 465,
    // secure: false,
    auth: {
        user: config.host_email,
        pass: config.host_email_pass
    }
})

exports.sendForgotPasswordMail = (email, url) => {

    return new Promise((resolve, reject) => {
        Email.findOne({
            subject: "Forgot Password"
        }).then(async template => {
            debug(template);
            const mailOptions = {
                from: config.host_email,
                to: email,
                subject: 'Reset Password Link',
                html: format(template.content, { url })
            }
            let mail = await transporter.sendMail(mailOptions);
            debug(mail)
            resolve(mail);
        }).catch(error => {
            debug(error);
            reject(error)
        })
    });
}
