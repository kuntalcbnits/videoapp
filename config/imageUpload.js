{
    const multer = require("multer");
    const fs = require('fs');
    const config = require("../config/config");

    var userImage = multer.diskStorage({
        destination: (req, file, cb) => {
            const uploadDir = __basedir + config.userImageRelativePath;
            fs.exists(uploadDir, (exists) => {
                if (exists) {
                    cb(null, uploadDir);
                } else {
                    fs.mkdir(uploadDir, (err) => {
                        if (err) {
                            cb(err, null)
                        } else {
                            cb(null, uploadDir)
                        }
                    })
                }
            });
        },
        filename: (req, file, cb) => {
            cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname);
        }
    });
    exports.uploadUserImage = multer({
        storage: userImage
    }).fields([{ name: "personalID" }]);

    var driverImage = multer.diskStorage({
        destination: (req, file, cb) => {
            const uploadDir = __basedir + config.driverImageRelativePath;
            fs.exists(uploadDir, (exists) => {
                if (exists) {
                    cb(null, uploadDir);
                } else {
                    fs.mkdir(uploadDir, (err) => {
                        if (err) {
                            cb(err, null)
                        } else {
                            cb(null, uploadDir)
                        }
                    })
                }
            });
        },
        filename: (req, file, cb) => {
            cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname);
        }
    });
    exports.uploadDriverImage = multer({
        storage: driverImage,
    }).fields([{ name: "profileImg" }, { name: "personalID" }, { name: "drivingLicence" }]);

    var siteSettingImage = multer.diskStorage({
        destination: (req, file, cb) => {
            const uploadDir = __basedir + config.siteSettingRelativePath;
            fs.exists(uploadDir, (exists) => {
                if (exists) {
                    cb(null, uploadDir);
                } else {
                    fs.mkdir(uploadDir, (err) => {
                        if (err) {
                            cb(err, null)
                        } else {
                            cb(null, uploadDir)
                        }
                    })
                }
            });
        },
        filename: (req, file, cb) => {
            cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname);
        }
    });
    exports.uploadSiteSettingImage = multer({
        storage: siteSettingImage,
    }).fields([{ name: "iconFile" }, { name: "logoFile" }]);

    var vehicleImage = multer.diskStorage({
        destination: (req, file, cb) => {
            const uploadDir = __basedir + config.vehicleImageRelativePath;
            fs.exists(uploadDir, (exists) => {
                if (exists) {
                    cb(null, uploadDir);
                } else {
                    fs.mkdir(uploadDir, (err) => {
                        if (err) {
                            cb(err, null)
                        } else {
                            cb(null, uploadDir)
                        }
                    })
                }
            });
        },
        filename: (req, file, cb) => {
            cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname);
        }
    });
    exports.uploadVehicleImage = multer({
        storage: vehicleImage
    }).fields([{ name: "registrationCertificate" }, { name: "vehicleInsurance" }, { name: "noc" }, { name: "fitnessCertificate" }])
}
