const debug = require('debug')('utaxiu-api:verifyToken');
const Admin = require('../models/admin.model');
const Driver = require('../models/driver.model');
const config = require('../config/config');
const key = config.secret;

const jwt = require('jsonwebtoken');

exports.verifyAdmin = (req, res, next) => {
    let token;

    if (req.headers && !req.headers['x-authorization']) return res.status(401).send({
        message: "No Authorization header was found"
    });

    let parts = req.headers['x-authorization'].split(" ");
    if (parts.length !== 2) return res.status(401).send({
        message: "Format is Authorization: Bearer [token]"
    });

    let scheme = parts[0],
        credentials = parts[1];
    if (/^Bearer$/i.test(scheme)) {
        token = credentials;
    }

    jwt.verify(token, key, (err, decoded) => {
        if (err) {
            debug(err);
            return res.status(500).send(err).end()
        };

        Admin.findOne({ _id: decoded._id })
            .then(admin => {
                req.admin = admin;
                next();
            })
            .catch(error => {
                return res.status(500).send(error);
            });
    });
};

exports.verifyDriver = (req, res, next) => {
    let token;

    if (req.headers && !req.headers['x-authorization']) return res.status(401).send({
        message: "No Authorization header was found"
    });

    let parts = req.headers['x-authorization'].split(" ");
    if (parts.length !== 2) return res.status(401).send({
        message: "Format is Authorization: Bearer [token]"
    });

    let scheme = parts[0],
        credentials = parts[1];
    if (/^Bearer$/i.test(scheme)) {
        token = credentials;
    }

    jwt.verify(token, key, (err, decoded) => {
        if (err) {
            debug(err);
            return res.status(500).send(err).end()
        };
        debug(decoded);

        Driver.findOne({ _id: decoded._id })
            .then(driver => {
                req.driver = driver;
                debug(req.driver);
                next();
            })
            .catch(error => {
                return res.status(500).send(error);
            });
    });
};
