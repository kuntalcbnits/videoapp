var nodemailer = require('nodemailer');


exports.sendMyMail = (email, subject, message) => {
    try {
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'sudipto.ganguly@cbnits.com',
                pass: 'cbnits@1234'
            }
        });

        var mailOptions = {
            from: 'sudipto.ganguly@cbnits.com',
            to: email,
            subject: subject,
            text: message
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            }
            else {
                console.log('Email sent: ' + info.response);
            }
        });
    }
    catch (error) {
        console.log(error.message)
    }
};